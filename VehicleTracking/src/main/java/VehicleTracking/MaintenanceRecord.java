/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package VehicleTracking;

/**
 *
 * @author dusty_000
 */
public class MaintenanceRecord {
    String type;
    int odometer;
    float price;
    private short day;
    private short month;
    private short year;
    
    
    /*** SETTERS ***/
    void setMaintenanceEntry (String description, int odometerEntry, float priceEntry)
    {
        type = description;
        odometer = odometerEntry;
        price = priceEntry;
    }
    
    void setMaintenanceDate (short dateYear, short dateMonth, short dateDay)
    {
        year  = dateYear;
        month = dateMonth;
        day   = dateDay;
    }
    /*** END SETTERS ***/
    
    /*** GETTERS ***/
    String getMaintenanceType ()
    {
        return type;
    }
    
    int getMaintenanceOdometer ()
    {
        return odometer;
    }
    
    float getMaintenancePrice ()
    {
        return price;
    }
    
    String getDateStr (String joiner)
    {
        return String.join(joiner, Short.toString(year), Short.toString(month), Short.toString(day));
    }
    
    short[] getDate ()
    {
        short date[] = {year, month, day};
        return date;
    }
    /*** END GETTERS ***/
}
