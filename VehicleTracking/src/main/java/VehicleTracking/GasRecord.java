/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package VehicleTracking;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author dusty_000
 */
public class GasRecord {
    private boolean isFilledFull; /* 1= filled full, 0=partial fill */
    private Float fuelIn; /* U.S. Gallons */
    private int odometer; 
    private Float fillPrice; /* U.S. Dollars */
    private short day;
    private short month;
    private short year;
    
    String fuelRecord_filePath = System.getProperty("user.dir")+"\\Garage\\Fuel\\";
    
    /*** SETTERS ***/
    /**
     * Sets the log data needed to perform calculations
     * @param isFull true if the log entry was to tank fill (pump shutoff)
     * @param gallons fuel added in U.S. gallons
     * @param odometerEntry Odometer reading in Miles
     * @param price fill price in U.S. dollars
     */
    void setGasFillInfo (boolean isFull, Float gallons, int odometerEntry, Float price)
    {
        this.isFilledFull = isFull;
        this.fuelIn = gallons;
        this.odometer = odometerEntry;
        this.fillPrice = price;
    }
    
    void setGasFillInfo (String date, String isFull, String gallons, String odometerEntry, String price)
    {
        System.out.println("Vehicle.addGasEntry"+date);
        
        this.isFilledFull = Boolean.valueOf(isFull);
        this.fuelIn = Float.valueOf(gallons);
        this.odometer = Integer.parseUnsignedInt(odometerEntry);
        this.setGasLogDate(date);
        if(price.contains("$"))
        {
            price = price.replace("$","");
        }
        if(!price.isBlank())
        {
            this.fillPrice = Float.valueOf(price);
        }
        else
        {
            this.fillPrice = null;
        }
    }
    
    /**
     * Sets the date of the log entry
     * @param dateYear 
     * @param dateMonth
     * @param dateDay 
     */
    void setGasLogDate (short dateYear, short dateMonth, short dateDay)
    {
        year  = dateYear;
        month = dateMonth;
        day   = dateDay;
    }
    /**
     * Sets the date of the log entry given a string with format yyyyMmDd or yyyy/mm/dd
     * @param yyyyMmDd 
     */
    void setGasLogDate (String mmDdYyyy)
    {
        System.out.println("Vehicle.addGasEntry "+mmDdYyyy);
        
        String regex;
        String[] dateStr = {"","",""};
        if (!mmDdYyyy.isBlank())
        {
            if (mmDdYyyy.length() > 8)
            {
                regex = Character.toString(mmDdYyyy.charAt(2));
                dateStr = mmDdYyyy.split(regex);
                
                this.month = Short.parseShort(dateStr[0]);
                this.day = Short.parseShort(dateStr[1]);
                this.year = Short.parseShort(dateStr[2]);
            }
            
            else
            {
                this.month = Short.parseShort(mmDdYyyy.substring(0, 2));
                this.day = Short.parseShort(mmDdYyyy.substring(2, 4));
                this.year = Short.parseShort(mmDdYyyy.substring(4, 8));             
            }
        }
        else
        {
            
        }
    }
    
    void setInfoFromFile(String carName) 
    {
        String fileName = fuelRecord_filePath.concat(carName+".csv");
        File file = new File(fileName);
        
        Scanner fs;
        try {
            fs = new Scanner(file);
            fs.useDelimiter(",");
            
            /*date,isFull,fuelVolume,Odometer,fillPrice*/
            this.setGasFillInfo(fs.next(), fs.next(), fs.next(), fs.next(), fs.next());
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Vehicle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*** END SETTERS ***/
    
    /*** GETTERS ***/
    /**
     * Gets the Full or partial fill info about the log entry
     * @return true if the log entry was to tank fill (pump shutoff)
     */
    boolean getIsFull ()
    {
        return isFilledFull;
    }
    
    /**
     * Gets the fuel added info of the log entry
     * @return fuel added in U.S. gallons
     */
    Float getFuelIn ()
    {
        return fuelIn;
    }
    
    /**
     * Gets the Odometer reading of the log entry
     * @return Odometer reading in Miles
     */
    int getOdometer ()
    {
        return odometer;
    }
    
    /**
     * Gets the fill price of the log entry
     * @return fill price in U.S. dollars
     */
    Float getFillPrice ()
    {
        return fillPrice;
    }
    
    /**
     * Gets the Date of the fill entry in String format
     * @param joiner the delimiter that separates each element (e.g. "-" or "/")
     * @return date in string format using the designated joiner
     */
    String getDateStr (String joiner)
    {
        String dayFiller = "";
        String monthFiller = "";
        if (this.day < 10)
        {
            dayFiller = "0";
        }else {dayFiller = "";}
        if (this.month < 10)
        {
            monthFiller = "0";
        }else{monthFiller ="";}
        
        return String.join(joiner, Short.toString(year), monthFiller + Short.toString(month), dayFiller + Short.toString(day));
    }
    
    /**
     * Gets the Date of the fill entry in an array of shorts
     * @return short array {year, month, day}
     */
    short[] getDate ()
    {
        short date[] = {year, month, day};
        return date;
    }
    /*** END GETTERS ***/
    
    void saveGasEntry (String carName)
    {
        /*date,isFull,fuelVolume,Odometer,fillPrice*/
        String fileName = fuelRecord_filePath.concat(carName+".csv");
        String line = String.join(",", 
                this.getDateStr("/"),
                String.valueOf(this.getIsFull()),
                Float.toString(this.getFuelIn()),
                Integer.toString(this.getOdometer()),
                Float.toString(this.getFillPrice())
                +"\n"
        );
        
        FileWriter csvWriter;
        try {
            csvWriter = new FileWriter(fileName,true);
            csvWriter.append(line);
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Vehicle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
